# Tangible AI Community Code

## Core Team Commitments and Benefits

**Benefits.** All core team members are eligible to:

- Receive symbolic honorarium, as described in &quot;Compensation&quot; section.
- Have visibility into the spending of 20% &quot;company tax&quot;:
  - Maria will publish quarterly P&amp;L, accessible to all team members.
  - Maria will present any expenses bigger than $300 in #core-team Slack channel or during all-hands meetings, for team members to voice concerns or objections.
  - Core team member can suggest an expense, privately with Maria or on #core-team slack channel
  - Core team member can ask to have a detailed expenses report of the company
- Have visibility into all Tangible AI&#39;s projects:
  - Every Tangible AI project will be published on core-team channel when the work on a proposal is starting, or earlier if possible
  - Core team members can apply to participate in every project or proposal that matches their skill level and receive priority in consideration.
- Be project/proposal owners and project managers of projects that match their skill level.
- Appear on the company&#39;s &quot;About us&quot; web page as a team member.
- Suggest changes and vote on changing company&#39;s processes - including this document
- Suggest and vote on accepting new team members into the Core Team.

**Duties and responsibility.** All core team members commit to **:**

- Participate in a company all-hands meeting once a month.
- Allocate time for open-source social impact projects.
- Attend at least 1 weekly 1:1 with either Hobson or Maria.
- Give priority consideration to Tangible AI projects.

## Compensation &amp; Profit Sharing

### Consulting contracts revenue distribution:

- Each consulting contract fees will be distributed according to the following scheme:

    - 20% goes to cover Tangible expenses and infrastructure investments
    - 20% goes to the retainer pool for core members
    - 10% goes to people who worked on the sale, and they split it however they see fit (they may decide to contribute it to implementation costs)
    - 50% goes to implementers.
- For FFP contracts, the pay for implementers is FFP. Each project&#39;s **project owner** is responsible for allocating compensation for implementers.

### Compensation process

- The company&#39;s contract revenue is going be tracked in the following document: [https://docs.google.com/spreadsheets/d/1-gx2gHKu2yJ96MibjS4\_bs17xp1H0EEdzvR5NhPqitQ/edit?usp=sharing](https://docs.google.com/spreadsheets/d/1-gx2gHKu2yJ96MibjS4_bs17xp1H0EEdzvR5NhPqitQ/edit?usp=sharing)
- Total compensation for each member will be calculated quarterly.
- Sales owners will submit sales commission allocation once the deal is signed.
- Project owners will submit implementer work allocation for every invoice issued by Tangible AI.
- Team members will submit to Maria an estimate of days per week spent on contributing to projects up to one week before the end of the quarter.
- Team members will submit an invoice for the sum calculated in the spreadsheet on the last week of every quarter.

### Types of Compensation

Core member&#39;s compensation will consist of one or more among the following compensation types:

- **Retainer/fellowship/honorarium** - retainer pool divided between the core members.
- **Sales Royalty:** 10% of every contract divided between people who secured the sale
- **Implementer Fees:** Are calculated per-project by the project owner according to the member&#39;s part in the implementation.
- **Internal Contracts:** Tangible AI may decide to issue a contract that contributes to the company foundations (such as: website improvement, social media), out of the shared 20% budget. The projects will be FFP; the amount will be paid in full to the contractor or core member working on the project.

## Tangible AI Leadership Priviledges

- Serve as arbiter every time there is disagreement
- Fire any core member
- Propose the spending for the 20% tax
- Scrap this agreement if it doesn&#39;t work after a trial period (September 30th 2021)
