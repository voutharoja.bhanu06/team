# 2021 spring projects

## Aditi - questioner bot (in qary or rasa)

* Torronto

### Week 2

- Squad named entity extraction from answer
- Squad find all sentences with named entity in them
- concatenate relevant sentences to create new shorter context
- create target question in second column
- include squad question ID as 3rd column or index for new dataset
- use NMT to translate from statement to question

## Vish - history tutor bot

* San Diego: -2 hr

### Week 2

- download/scrape some documents about greek history
- question answering on history document

## Camille - convoscript graph merge

San Diego: -2 hr

### Week 2

- run script_to_dialog()
- compare statements with cosine distance?
- merge 2 scripts

## Bhanu - BertSum and BigBird for long abstractive summarization

India (IIT?): +10.5 hr

### Week 2

## Susanna - rasa or qary chatbot

### Week 2

- write out back and forth script for desired application
- write alternative back and forth

## Wamani - django backend for playground.proai.org

Africa (Uganda?): +8 hr

### Week 2

- empty django app
- models.py file with fields for x and y

## Una - Product classifier (NLP)

### Week 2

- linear regression on product rating

## Josh - College course TA and student success predictor

Philadelphia? Maine?: +1 hr

### Week 2

- data transformation to sparse binary time series
