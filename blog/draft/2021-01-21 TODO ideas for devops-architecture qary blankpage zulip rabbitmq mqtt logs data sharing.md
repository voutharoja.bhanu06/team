# Publish-subscribe architecture ideas for chat apps

Need an architecture for qary, blankpage etc that uses

Python Chat DRF

## Pubsub router on digital ocean

### rabbitmq

- slow but flexible
- no peer-to-peer like ActiveMQ (Apache, C++ or Go implementations?)


- [general tutorial](https://www.rabbitmq.com/tutorials/tutorial-three-python.html)
- [installation on Digital Ocean Droplet](https://www.digitalocean.com/community/tutorials/how-to-install-and-manage-rabbitmq)

### mosquitto

EPL/EDL licensed - Eclipse Mosquitto is an open source () message broker that implements the MQTT protocol versions 5.0, 3.1.1 and 3.1

### ActiveMQ

Apache license - popular - allows peer-to-peer

### 0MQ

faster, low-latency - popular in finance for HFT and distributed computing -
only peer-to-peer so have to handle routing - asynchronous messaging library aimed at use in scalable distributed or concurrent applications. It provides a message queue without dedicated broker

